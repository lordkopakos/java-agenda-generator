package ioproject.jattributes;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

/**
 *Klasa rozszerza funkcjonalność JPanel o możliwość ustawienia tła jako obrazka. 
 * @author Dominik Kopaczka
 * @see <a href="https://docs.oracle.com/javase/7/docs/api/java/awt/package-summary.html">java.awt.*</a>
 * @see <a href="https://docs.oracle.com/javase/7/docs/api/javax/swing/package-summary.html">java.swing.*</a>
 * @version 1.0
 * @since   2016-01-02
 */
public class ImagePanel extends JPanel {

  private Image img;

    /**
     *Konstruktor klasy ImagePanel który pobiera obraz ze ścieżki podanej w argumencie i ustawia jako tło panelu. 
     * @param img Ścieżka do pliku z obrazem.
     */
    public ImagePanel(String img) {
    this(new ImageIcon(img).getImage());
  }

    /**
     *Konstruktor klasy ImagePanel który pobiera obraz ustawia jako tło panelu. 
     * @param img Obiekt kasy Image.
     */
    public ImagePanel(Image img) {
    this.img = img;
    Dimension size = new Dimension(img.getWidth(null), img.getHeight(null));
    setPreferredSize(size);
    setMinimumSize(size);
    setMaximumSize(size);
    setSize(size);
    setLayout(null);
    
  }

  @Override
  public void paintComponent(Graphics g) {
      
   super.paintComponent(g);  
    g.drawImage(img, 0, 0, null);
   
  }

}