/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ioproject;

import convert.ConvertFile;
import ioproject.jattributes.ImagePanel;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JTextField;
import javax.swing.border.Border;

/**
 * Klasa implementująca interfejs I_View, ActionListener.
 * Tworzy widok programu i zapewnia łatwą synchronizację z resztą projektu.
 * Wystarczy wywołać "ApplicationView view=new ApplicationView();", aby cały projekt 
 * zamieścić jako w większym projekcie. Stosuje autorską wersję wzorca projektowego 
 * MVC.
 * @see I_View
 * @see convert.ConvertFile
 * @see ioproject.jattributes.ImagePanel
 * @see <a href="https://docs.oracle.com/javase/7/docs/api/java/awt/package-summary.html">java.awt.*</a>
 * @see <a href="https://docs.oracle.com/javase/7/docs/api/javax/swing/package-summary.html">java.swing.*</a>
 * @author Dominik Kopaczka
 * @version 1.0
 * @since   2016-01-02
 */
public final class ApplicationView extends JFrame implements I_View, ActionListener {

    private ImagePanel imageMainPanel;
    private ConvertFile convertFile;

    private JButton btnVertical;
    private JButton btnHorizontal;
    private JButton btnGenerateAgenda;

    private JTextField txtFileName;

    private int widthTextField;
    private int heightTextField;
    private int xSizeLabel;
    private int xSizeTextField;
    private int first;

    /**
     *Konstruktor klasy ApplicationView ustawia wielkość komponentów swing, a następnie je inicjalizuje. 
     */
    public ApplicationView() {
        setComponentSize();
        init();
    }

    private void setComponentSize() {
        widthTextField = 300;
        heightTextField = 50;
        xSizeLabel = 100;
        xSizeTextField = 200;
        first = 50;
    }

    /**
     *Metoda inicjalizująca cały widok programu. 
     */
    @Override
    public void init() {
        convertFile = ConvertFile.getInstance();
        initFrame();
        initMainPanel();
        initAllComponent();
        initFields();
        getLayeredPane().add(imageMainPanel, JLayeredPane.MODAL_LAYER);
    }

    /**
     *Metoda inicjalizuje okno programu. 
     */
    @Override
    public void initFrame() {
        setBounds(100, 100, 1200, 800);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);
        setVisible(true);
    }

    /**
     *Metoda inicjalizuje główny panel programu. 
     */
    @Override
    public void initMainPanel() {
        imageMainPanel = new ImagePanel("images/gui/background.jpg");
    }

    /**
     *Metoda inicjalizuje komponenty swing(przyciski i pola wyboru). 
     */
    @Override
    public void initAllComponent() {
        initComboBoxes();
        initButtons();
    }

    /**
     *Metoda inicjalizuje pola wyboru.
     */
    @Override
    public void initComboBoxes() {

        Border border
                = BorderFactory.createLineBorder(new Color((float) 0.9, (float) 0.4, (float) 0.02), 5);
        ConvertFile.ConvertType[] convertType = ConvertFile.ConvertType.values();
        JComboBox convertComboBox = new JComboBox(convertType);
        setCombo(0, border, "Select Convert Type", convertComboBox);
        imageMainPanel.setLocation(0, 0);
        imageMainPanel.add(convertComboBox);
    }

    /**
     *Metoda inicjalizuje przyciski.
     */
    @Override
    public void initButtons() {
        ImageIcon imgVertical = loadImage("vertical.jpg");
        ImageIcon imgHorizontal = loadImage("horizontal.jpg");
        ImageIcon imgGenerateFile = loadImage("generateFile.png");

        btnVertical = new JButton();
        addButton(btnVertical, imgVertical, 500, 50, 595, 655);
        imageMainPanel.add(btnVertical);

        btnHorizontal = new JButton();
        addButton(btnHorizontal, imgHorizontal, 500, 50, 605, 381);
        btnHorizontal.setVisible(false);
        imageMainPanel.add(btnHorizontal);

        btnGenerateAgenda = new JButton();
        addButton(btnGenerateAgenda, imgGenerateFile, 100, 480, 301, 65);
        imageMainPanel.add(btnGenerateAgenda);

    }

    /**
     *Metoda inicjalizuje pola tektowe i etykiety je opisujące.
     */
    @Override
    public void initFields() {
        JLabel lbfileName = new JLabel("File Name:");
        JLabel lbNumberOfAgendaCopy = new JLabel("Number Of Agenda Copy:");

        txtFileName = new JTextField();

        addField(lbfileName, txtFileName, 1, "Select File Name");

        imageMainPanel.add(lbfileName);
        imageMainPanel.add(txtFileName);
        imageMainPanel.add(lbNumberOfAgendaCopy);
    }

    private void setCombo(int witch, Border border, String toolTipText, JComboBox combo) {

        combo.setSize(widthTextField, heightTextField);
        combo.setLocation(100, first + witch * 140);
        combo.setBorder(border);
        combo.setToolTipText(toolTipText);
        combo.setFont(new Font("Times New Roman", Font.PLAIN, 20));
        combo.setOpaque(false);
        combo.addActionListener(combo);
    }

    private ImageIcon loadImage(String name) {
        String filename = "images/gui/" + name;
        return new ImageIcon(filename);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object src = e.getSource();
        if (src == btnVertical) {
            btnVertical.setVisible(false);
            btnHorizontal.setVisible(true);
            convertFile.setArrangementState(ConvertFile.ArrangementType.HORIZONTAL);
        } else if (src == btnHorizontal) {
            btnVertical.setVisible(true);
            btnHorizontal.setVisible(false);
            convertFile.setArrangementState(ConvertFile.ArrangementType.VERTICAL);
        } else if (src == btnGenerateAgenda) {
            generateAgenda();
        }
    }

    private void generateAgenda() {
        convertFile.setFileName(txtFileName.getText());
        int numberOfThreads = setNumberOfThreadsFromTextField();
        for (int threadIndex = 0; threadIndex < numberOfThreads; threadIndex++) {
            new Thread(convertFile).start();
        }
    }

    private int setNumberOfThreadsFromTextField() {
        return 1;
    }

    private void addField(JLabel jlabel, JTextField textField, int witch, String toolTipText) {

        Border border
                = BorderFactory.createLineBorder(new Color((float) 0.9, (float) 0.4, (float) 0.02), 5);

        jlabel.setSize(xSizeLabel, heightTextField);
        jlabel.setLocation(100, first + witch * 140);
        jlabel.setFont(new Font("Times New Roman", Font.PLAIN, 20));
        jlabel.setForeground(new Color((float) 0.9, (float) 0.4, (float) 0.02));

        textField.setSize(xSizeTextField, heightTextField);
        textField.setLocation(100 + xSizeLabel, first + witch * 140);
        textField.setOpaque(false);
        textField.setBorder(border);
        textField.setToolTipText(toolTipText);
        textField.setFont(new Font("Times New Roman", Font.PLAIN, 20));
        textField.addActionListener(this);
        textField.setForeground(new Color((float) 0.9, (float) 0.4, (float) 0.02));

    }

    /**
     *Metoda ustawiająca rozmiar etykiety względem osi X.
     * @param xSizeLabel Rozmiar etykiety względem osi X.
     */
    public void setxSizeLabel(int xSizeLabel) {
        this.xSizeLabel = xSizeLabel;
    }

    /**
     *Metoda ustawiająca rozmiar pola tekstowego względem osi X.
     * @param xSizeTextField Rozmiar pola tekstowego względem osi X.
     */
    public void setxSizeTextField(int xSizeTextField) {
        this.xSizeTextField = xSizeTextField;
    }

    private void addButton(JButton button, ImageIcon imageicon, int locationX, int locationY, int sizeX, int sizeY) {
        button.setIgnoreRepaint(true);
        button.setFocusable(false);
        button.setBorder(null);
        button.setContentAreaFilled(false);
        button.setIcon(imageicon);
        button.setLocation(locationX, locationY);
        button.setSize(sizeX, sizeY);
        button.setVisible(true);
        button.addActionListener(this);
    }

}
